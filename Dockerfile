FROM ubuntu:22.04 AS installer

RUN apt-get update -qq && \
    apt-get install -qqy wget xz-utils && \
    apt-get clean -qq && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /download
# "Downloading any of these files constitutes as acceptance of the license agreement.""
# http://numberworld.org/y-cruncher/license.html
ENV RELEASE_NAME="y-cruncher v0.7.10.9513-dynamic"
RUN wget -q "http://numberworld.org/y-cruncher/${RELEASE_NAME}.tar.xz" &&\
    echo "6ded4fa4e975c92fc7cb7a2fb3a64ce5ff8d8e1ed5a3a32d3e7a882b344f40bf ${RELEASE_NAME}.tar.xz" | sha256sum --check --status && \
    tar xvf "${RELEASE_NAME}.tar.xz" && \
    mv "${RELEASE_NAME}" /y-cruncher

FROM ubuntu:22.04 as runner

RUN apt-get update -qq && \
    apt-get install -qqy numactl libtbb-dev && \
    apt-get clean -qq && \
    rm -rf /var/lib/apt/lists/*

COPY --from=installer /y-cruncher /y-cruncher
WORKDIR /y-cruncher
ENV LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:/y-cruncher/Binaries"
ENTRYPOINT [ "./y-cruncher" ]