# Validating Pi

This repository tracks a project to verify the many different places websites quote values of Pi, e, and other important mathematical constants.
In numbers with millions of digits, the likelihood that a person verifies the correctness of these digits is low (and in many cases, the likelihood that an erroneously missing digit affects your calculation even lower).
Still, the digits of these constants are verifiable, and it's the principle of the thing anyway.

## Methodology

The approach to this is fairly simple.
Using [y-cruncher](http://numberworld.org/y-cruncher/), we can calculate many digits of several important mathematical constants.
This software has been used to break many world records for 'most digits calculated' and, while it's possible some flaw causes some random error every billion digits, we can assume it as a reference point.

With a trustworthy reference constant in hand, we can simply search "digits of Pi" and download a whole bunch of reproductions from around the internet.
For each source, we can download/extract the reported values of Pi, and compare the checksum of that fetched value with that of the reference value.
In case of disagreeing checksums -- proving there is a difference -- we can investigate further to find where, and how many, discrepancies there are.

## License

This project uses y-cruncher, which has its own [license](https://www.numberworld.org/y-cruncher/license.html).
By using the code listed here, you are implicitly also agreeing to y-cruncher's terms.

Copyright © 2023 Andrew Fiorillo

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.