
# The number of digits to calculate in our reference. Should be larger than all of the sampled reproductions.
DECIMAL_DIGITS := 1m

build:
	BUILDKIT=1 docker build -q -t y-cruncher .

bench: build
	docker run --rm -it y-cruncher skip-warnings bench 100m

reference/pi: build
	mkdir -p reference/pi
	docker run --rm -it -v "$$(pwd)/reference/pi:/output" --user $$(id -u):$$(id -g) \
		y-cruncher custom pi -dec:$(DECIMAL_DIGITS) -o "/output" -verify:1