# Piday.org

**Link**: https://www.piday.org/million/

This is an HTML page that simply promises to deliver 1M digits of Pi.
It does this infinite scolling, so extracting the digits is nontrivial.
This is provided by [Mometrix](https://www.mometrix.com/), a test prep company.

## Extraction Notes

By scrolling through the page, we see network requests to fetch the new data of the format:

```bash
curl 'https://www.piday.org/wp-json/millionpi/v1/million?action=example_ajax_request&page=20' \
  -H 'accept: */*' \
  -H 'referer: https://www.piday.org/million/' \
  --compressed
```

And the final page is 20.
The result of each call is just a string containing the digits.
So `curl ...&page=1` looks like

```
"3.14159..."
```

And `curl ...&page=20` looks like

```
"...58151"
```

## Conclusion

It matches!
