#!/usr/bin/env python
from hashlib import sha256
from pathlib import Path

import requests

BASE_URL = 'https://www.mathsisfun.com/numbers/images/pi-million.txt'

LOCAL_COPY = Path(__file__).parent.joinpath('piday_1m.txt')
REFERENCE = Path(__file__).parent.parent.parent.joinpath('reference/pi/Pi - Dec - Chudnovsky.txt').resolve()

session = requests.Session()
session.headers = {
    'accept': '*/*',
    'user-agent': 'https://www.mathsisfun.com/numbers/pi-digits.html'
}

def extract() -> str:
    all_pi = session.get(BASE_URL).text
    return all_pi

def matches(extracted: str, reference: str) -> bool:
    extracted_shasum = sha256(extracted.encode()).hexdigest()
    reference_shasum = sha256(reference[:len(extracted)].encode()).hexdigest()
    return extracted_shasum == reference_shasum

def main():
    """
    The entrypoint if running this script on its own
    """
    if LOCAL_COPY.exists():
        print('local copy exists, loading...')
        all_pi = LOCAL_COPY.read_text()
    else:
        all_pi = extract()
        LOCAL_COPY.write_text(all_pi)
        print(f'wrote {len(all_pi)} chars to {LOCAL_COPY}')

    reference = REFERENCE.read_text()
    if matches(all_pi, reference):
        print('matches reference!')
    else:
        print('discrepancy!')

if __name__ == "__main__":
    main()