# Math Is Fun

**Link:** https://www.mathsisfun.com/numbers/pi-digits.html

This is an HTML page provided by [Rod Pierce](https://www.mathsisfun.com/aboutmathsisfun.html) alongside other educational resources on mathematics.
The dialogue box allows selecting how many digits of pi, and then essentially downloading them as a text file from a server. 

## Extraction.

It's a simple text file, so we can make a single request like so, and then compare the digits.

```
curl 'https://www.mathsisfun.com/numbers/images/pi-million.txt' \
  -H 'accept: */*' \
  -H 'referer: https://www.mathsisfun.com/numbers/pi-digits.html' \
  --compressed
```

## Conclusion

It matches!
